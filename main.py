# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
from dash import dcc
from dash import html
import plotly.express as px
import pandas as pd

app = dash.Dash(__name__)
server = app.server


flights = pd.read_csv("flights.csv.gz")
airports = pd.read_csv("airports.csv")


#Name is Fig_out
#figure out to be Total number of flights deaparture in each airport
#-------------------------------------------------------------------------------------------------------

fig_out = None
a = flights.groupby(['origin']).size()
b = a.sort_values(ascending=False)
c = b.to_frame()
c = c.rename(columns = {0:'num'})
new_t = c.reset_index( level = 0 )
new_t
# fig = px.bar(new_t, x='origin', y='num')

fig = px.bar(new_t, x='origin', y='num',labels={
                     "origin": "Airport codes",
                     "num": "Total departure",
                 },
                title="Total number of flights departure in each airport")
fig_out = fig




#-------------------------------------------------------------------------------------------------------

#Name is fig_day
#figure out to be Flights in date of each month
#-------------------------------------------------------------------------------------------------------

fig_day = None
a = flights.groupby(['day']).size()
c = a.to_frame()
c = c.rename(columns = {0:'num'})
new_t = c.reset_index( level = 0 )
fig = px.bar(new_t, x='day', y='num',labels={
                     "day": "Date (1-31)",
                     "num": "Flight count",
                 },
                title="Flights in date of each month")
fig_day = fig
fig


#-------------------------------------------------------------------------------------------------------

# df = pd.DataFrame({
#     "Fruit": ["Apples", "Oranges", "Bananas", "Apples", "Oranges", "Bananas"],
#     "Amount": [4, 1, 2, 2, 4, 5],
#     "City": ["SF", "SF", "SF", "Montreal", "Montreal", "Montreal"]
# })

# fig = px.bar(df, x="Fruit", y="Amount", color="City", barmode="group")

# df2 = pd.read_csv('https://gist.githubusercontent.com/chriddyp/5d1ea79569ed194d432e56108a04d188/raw/a9f9e8076b837d541398e999dcbac2b2826a81f8/gdp-life-exp-2007.csv')

# fig2 = px.scatter(df2, x="gdp per capita", y="life expectancy",
#                  size="population", color="continent", hover_name="country",
#                  log_x=True, size_max=60)

app.layout = html.Div(children=[
    html.H1(children='PS 6'),

    html.Div(children='''
        This is a diagram that shows the number of flights departure from each airport. We can see that the EWR is the hottest airport among there three airports. The visitors may tend to choose this airport for some certian reason. And LGA is the airports with the least nmber of departures, it may due to the location of the airport or some problems of the airport which need to be solved.
    '''),

    dcc.Graph(
        id='Total number of flights departure in each airport',
        figure=fig_out
    ),

    html.Div(children='''
        This shows that the number of flights in each date of a month are almost the same, which implies that the number of people travel by plane are almost the same for each date in a month. It's possible that people do not have obvious preference of travelling date in a month.
    '''),

    dcc.Graph(
        id='Flights in date of each month',
        figure=fig_day
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
